package com.home.java8.functional_interface.predicate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

public class DemoTest {

	Demo demo = new Demo();

	@Test
	public void getData() {
		assertTrue(true);
		List<Integer> al = new ArrayList<Integer>();
		al.add(10);
		al.add(15);
		al.add(50);
		al.add(25);
		al.add(30);
	}

	@Test
	public void getList() {
		List<Integer> al = new ArrayList<Integer>();
		al.add(10);
		// al.add(15);
		al.add(50);
		// al.add(25);
		al.add(30);
		assertEquals(al, demo.getList());
	}

}
