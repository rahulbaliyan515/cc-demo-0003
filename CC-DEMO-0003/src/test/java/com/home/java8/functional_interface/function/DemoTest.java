package com.home.java8.functional_interface.function;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class DemoTest {

	Demo demo = new Demo();

	@Test
	public void getLength() {
		assertEquals(4, demo.getLength());

	}

}
