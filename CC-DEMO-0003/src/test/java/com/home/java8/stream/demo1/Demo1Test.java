package com.home.java8.stream.demo1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

public class Demo1Test {

	Demo1 demo = new Demo1();

	@Test
	public void getCount() {
		assertEquals(3, demo.getCount());

	}

	@Test
	public void getList() {
		List<Integer> al = new ArrayList<Integer>();
		al.add(11);
		al.add(16);
		al.add(31);
		al.add(16);
		al.add(51);
		al.add(21);
		assertEquals(al, demo.getList());

	}

	@Test
	public void getSortedAscList() {
		List<Integer> al = new ArrayList<Integer>();
		al.add(10);
		al.add(15);
		al.add(15);
		al.add(20);
		al.add(30);
		al.add(50);
		assertEquals(al, demo.getSortedAscList());

	}

	@Test
	public void getSortedDscList() {
		List<Integer> al = new ArrayList<Integer>();
		al.add(50);
		al.add(30);
		al.add(20);
		al.add(15);
		al.add(15);
		al.add(10);

		assertEquals(al, demo.getSortedDscList());
	}

	@Test
	public void getMin() {
		assertEquals(10, demo.getMin());
	}

	@Test
	public void getMax() {
		assertEquals(50, demo.getMax());
	}
}
